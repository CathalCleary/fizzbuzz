package com.cathal.fizzbuzz;

import java.util.HashMap;
import java.util.Map;
/**
 * 
 * @author Cathal
 * FizzBuzz 
 */
public class FizzBuzz {
	
	public static void fizzBuzz(final Map<Integer, String> mapNumberToMessage){
		StringBuilder sb = new StringBuilder();
		
		for(int i = 1; i <= 100; i++){
			for(Map.Entry<Integer, String> entry: mapNumberToMessage.entrySet()){
				if(i % entry.getKey() == 0){
					sb.append(entry.getValue());
				}
			}
			
			if(sb.length() == 0){
				sb.append(i);
			}
			
			System.out.println(sb);
			sb.setLength(0);
		}
	}
	
	public static void main(String [] args){
		Map<Integer, String> mapNumberToMessage = new HashMap<Integer, String>();
		
		mapNumberToMessage.put(3, "fizz");
		mapNumberToMessage.put(5, "buzz");
		mapNumberToMessage.put(7, "apple");
		
		fizzBuzz(mapNumberToMessage);
	}

}
